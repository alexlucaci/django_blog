from django.contrib import admin
from .models import Article as ArticleModel

# Register your models here.


@admin.register(ArticleModel)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ("title", "author", "created_at")