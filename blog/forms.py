from django.forms import ModelForm

from .models import Article as ArticleModel


class ArticleForm(ModelForm):
    class Meta:
        model = ArticleModel
        fields = ["title", "category", "author", "content"]