from datetime import datetime
from django.test import TestCase
from django.utils import timezone

from blog.models import Article as ArticleModel

# Create your tests here.


class ArticleTest(TestCase):
    def test_article_created_success(self):
        ArticleModel.objects.create(title="Test article", category="test category", author="Test Author", content="Test content", created_at=datetime.now(tz=timezone.utc))
        article = ArticleModel.objects.get(title="Test article")
        self.assertEqual(article.category, "test category_")


class BlogPagesTest(TestCase):
    def test_home_page_content(self):
        res = self.client.get("/blog/")
        print(type(res.content))
        self.assertEqual(res.content, b"Welcome to my Blog")




