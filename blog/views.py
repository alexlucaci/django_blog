from datetime import datetime

from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse, HttpResponseNotFound
from django.views import View
from django.utils import timezone

from blog.models import Article as ArticleModel
from blog.forms import ArticleForm

# Create your views here.


class Home(View):
    def get(self, request):
        return HttpResponse("Welcome to my Blog")

    def post(self, request):
        return HttpResponse("[POST] Welcome to my BLOG")


class Article(View):
    def get(self, request):
        articles = ArticleModel.objects.all()

        return render(
            request,
            "articles.html",
            {"articles": articles, 'form': ArticleForm()}
        )

    def post(self, request):
        form = ArticleForm(request.POST)
        form.instance.created_at = datetime.now(tz=timezone.utc)
        form.save()
        return redirect("/blog/articles")


class ArticleDetails(View):
    def get(self, request, id):
        try:
            article = ArticleModel.objects.get(id=id)
        except ArticleModel.DoesNotExist:
            return HttpResponseNotFound()
        return render(request, "article_details.html", {"article": article})
